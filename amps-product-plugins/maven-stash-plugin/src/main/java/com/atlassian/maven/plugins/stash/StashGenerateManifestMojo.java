package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "generate-manifest")
public class StashGenerateManifestMojo extends GenerateManifestMojo
{
}
